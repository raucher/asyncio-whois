#! /usr/bin/python3.5

import argparse
import asyncio
import fileinput
import os
import sys

from whois_async.runners import QueuedRunner
from whois_async.writers import ListContextManager



d = """
WHOIS tool for address / list of addresses (only TCP-IPV4 info is returned)
Adresses could be specified as list of params: 
'whois.py whois_main.py python.org:80 whois.arin.net:43'
Or specified as parameter 'whois_main.py -f addrs.txt'
"""

parser = argparse.ArgumentParser(description=d)

parser.add_argument(dest='addresses',
                    metavar='address',
                    nargs='*')

parser.add_argument('-f', '--input',
                    dest='inputfile',
                    action='store',
                    help='file with list of addresses')

parser.add_argument('-o', '--output',
                    dest='outputfile',
                    action='store', )

args = parser.parse_args()


read_from = None
write_to = sys.stdout
dir_name = os.path.dirname(os.path.abspath(__file__))

###########################################
## Choose the place to read addresses from
################################################

# wrap list of addresses (if present) into context-manager
if args.addresses:
    read_from = ListContextManager(args.addresses)

# pass input filename (if present) to fileinput.input()
elif args.inputfile:
    input_abs_path = os.path.join(dir_name, args.inputfile)
    read_from = fileinput.input(input_abs_path)
else:
    parser.print_help()
    raise SystemExit()

# output
if args.outputfile:
    output_abs_path = os.path.join(dir_name, args.outputfile)
    write_to = open(output_abs_path, 'w')


if __name__ == '__main__':
    runner = QueuedRunner(
        writer_params={
            'indent': 4,
            'fp': write_to,
        },
    )
    
    with read_from as f:
        try:
            runner.run(read_from=f)
        finally:
            runner.close()
