import asyncio

from .writers import JsonLineWriter
from .providers import WhoisInfoProvider


class QueuedRunner:
    _queue = None
    _writer_class = JsonLineWriter
    _queue_size = 0

    num_workers = 3
    addrs_per_worker = 10

    @property
    def queue_size(self):
        return self.num_workers * self.addrs_per_worker


    def __init__(self, num_workers=3, addrs_per_worker = 10, writer=None, writer_params={}, loop=None):

        self._num_workers = num_workers
        self._addrs_per_worker = addrs_per_worker

        self._writer = writer or self._writer_class(**writer_params)
        self._queue = asyncio.Queue(maxsize=self.queue_size)
        self._loop = loop or asyncio.get_event_loop()

        self._whois_info = WhoisInfoProvider(loop=self._loop)

    
    def run(self, read_from): 
        """
        Runs event loop with main coroutine
        """       
        self._reader = read_from

        return self._loop.run_until_complete(self._main_coro())
        

    def close(self):
        self._loop.close()


    async def _main_coro(self):
        """
        Main coroutine which runs all the stuff
        """

        with self._writer as w:
            # compose list of consumers + producer
            coros = [self._queue_consumer() for _ in range(self.num_workers)]
            # coros.append(queue_feeder(queue, self._reader))
            coros.append(self._queue_feeder())

            # run them until done
            complete, pending = await asyncio.wait(coros)

            return (complete, pending)
    

    async def _queue_feeder(self):
        """
        Feeds the Queue
        """
        q = self._queue

        with self._reader as f_input:

            for line in (l.strip() for l in f_input):
                # drop comments
                if line.startswith('#'):
                    continue

                # get host:port address, :80 by default
                host, *port = line.split(':')
                port = int(port[0]) if port else 80
                
                # enqueue address
                await q.put((host, port))

        # wait for consumers
        q.join()

        # set sentinel for each consumer to signal end of work
        for _ in range(self._num_workers):
            await q.put(None)


    async def _queue_consumer(self):
        """
        Pulls out data from the queue until sentinel is encountered
        """
        q = self._queue
        writer = self._writer


        while True:
            addr = await q.get()
            # exit on sentinel value
            if addr is None:
                return

            info = await self._whois_info.get_info(addr)

            writer.write(info)

            q.task_done()
