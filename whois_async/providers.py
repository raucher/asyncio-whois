import asyncio
import socket
import re

class WhoisInfoProvider:
    """
    Gets WHOIS info for the address
    """
    address_format = '{host}:{port}'
    socket_family = socket.AF_INET
    socket_proto = socket.IPPROTO_TCP
    
    _whois_service = ("whois.arin.net", 43)
    _loop = asyncio.get_event_loop()


    def __init__(self, whois_srv_addr=None, addr_fmt=None, loop=None):

        self._loop = loop or self._loop
        self._whois_service = whois_srv_addr or self._whois_service
        self.address_format = addr_fmt or self.address_format


    async def get_info(self, address):
        """
        Produces dicitionary with purified WHOIS data of the address
        """
        loop = self._loop
        result = {}

        # add adress to dict as the key
        fmt_addr = self._address_formatter(address)
        addr_dict = result[fmt_addr] = {}
        
        # get addr_info for address
        addr_info = await self._get_addr_info(address)

        # add dict with key for each host which belongs to that address
        for resolved_addr in addr_info:
            # get whois data for each resolved host
            whois_data = await self._get_whois_data(resolved_addr)

            # clear it and make dictionary from plain text data
            whois_data = self._clear_whois_data(whois_data)
            whois_dict = self._make_whois_data_dict(whois_data)

            # add whois info dict for each host
            fmt_resolved_addr = self._address_formatter(resolved_addr)
            addr_dict[fmt_resolved_addr] = whois_dict

        return result


    async def _get_addr_info(self, addr):
        info = await self._loop.getaddrinfo(*addr, 
                                      family=self.socket_family, 
                                      proto=self.socket_proto)
        addresses = [item[4] for item in info]

        return addresses


    async def _get_whois_data(self, addr):
        # connect to whois server
        reader, writer = await asyncio.open_connection(*self._whois_service)

        ip, port = addr

        # send request
        request = '{ip}\r\n'.format(ip=ip)
        writer.write(request.encode())
        await writer.drain()

        # read response
        response = bytearray()
        while True:
            chunk = await reader.read(128)
            if not chunk:
                # close connection and exit
                writer.close()
                break

            response.extend(chunk)

        # return result
        return bytes(response).decode()

    def _address_formatter(self, address):
        """
        Formats addresses for consistency
        """
        host, port = address

        return self.address_format.format(host=host, port=port)

    def _make_whois_data_dict(self, data):
        match_re = re.compile(r'(\w+):\s*(.*)')

        matches = (match_re.match(l) for l in data.splitlines())
        result = dict(m.groups() for m in matches if m)
        
        return result

    def _clear_whois_data(self, data):
        cleared_data = []

        # split data in lines
        for line in data.splitlines():
            line = line.strip()
            # ignore lines starting with '#' and blank ones
            if line.startswith('#') or not line:
                continue

            cleared_data.append(line)
        
        # return actual data without junk
        return '\n'.join(cleared_data)
