import sys
import json

class BaseWriter:
    """
    Wraps file pointer and exposes context manager
    """
    def __init__(self, fp=sys.stdout):
        self.fp = fp

    def write(self, data):
        print(data, file=self.fp)

    def close(self):
        self.fp.close()

    def __enter__(self):
        return self

    def __exit__(self, *exc):
        self.close()

class PrintWriter(BaseWriter):
    def __init__(self):
        self.fp = sys.stdout

class JsonLineWriter(BaseWriter):
    indent = None
    fmt = '{}'

    def __init__(self, indent=None, *args, **kwargs):
        self.indent = indent
        super().__init__(*args, **kwargs)

    def write(self, data):
        json_data = self.fmt.format(
            json.dumps(data, indent=self.indent)
        )

        super().write(json_data)

class JsonStreamWriter(JsonLineWriter):
    fmt = '{},'

    def __enter__(self):
        # write open bracket '['
        self.fp.write('[')

        return super().__enter__()

    def __exit__(self, *exc):
        # replace trailing comma with enclosing bracket ']'
        if self.fp.seekable():
            pos = self.fp.tell()
            # go before trailing comma + \n
            self.fp.seek(pos-2, 0)
        
        self.fp.write(']')

        return super().__exit__(*exc)

class ListContextManager(list):
    def __enter__(self):
        return self

    def __exit__(self, *exc):
        pass
